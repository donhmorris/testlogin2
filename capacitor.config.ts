import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'testLogin4',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
