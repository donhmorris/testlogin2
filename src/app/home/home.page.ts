import {Component, OnInit} from '@angular/core';
import {ToastController} from '@ionic/angular';
import {initializeApp} from 'firebase/app';
import {getAuth, onAuthStateChanged, signInWithEmailAndPassword, signOut} from 'firebase/auth';

const fireConfig =
  {
    apiKey: "AIzaSyBST-GRdz47-jnsISqBKlhO8wBYMNGwQwI",
    authDomain: "test-login-4793c.firebaseapp.com",
    databaseURL: "https://test-login-4793c-default-rtdb.firebaseio.com",
    projectId: "test-login-4793c",
    storageBucket: "test-login-4793c.appspot.com",
    messagingSenderId: "300719414048",
    appId: "1:300719414048:web:712ce9b2be932adf3a1545"
  };

const fire = initializeApp( fireConfig );

@Component( {
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
} )
export class HomePage implements OnInit {
  email: string;
  pwd = '';
  isLoggedIn = false;

  constructor( protected toast: ToastController ) {
  }

  ngOnInit(): void {
    onAuthStateChanged( getAuth(), user => {
      if ( user ) {
        this.email = user.email;
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    } );
  }

  async doLogin() {
    try {
      const auth = getAuth();
      await signInWithEmailAndPassword( auth, this.email, this.pwd );
    } catch ( err ) {
      const toast = await this.toast.create( {
        header: 'Login Error', message: err.message, duration: 2500,
        color: 'danger', position: 'top'
      } );
      toast.present();
    }
  }

  async doLogout() {
    await signOut( getAuth() );
  }
}




















